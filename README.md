# Lost in translation
The very best translation app ever created.

# Known bugs
Because of the time crunch due to interviews, we were not able to finish in time.

# API
https://js-noroff-api.herokuapp.com/

## How to use
 - Download repository
 - Install node js
 - npm install react etc
 - Open app in IDE  
 - run "npm start" in console


### Co-Authored
Truls Hafnor and Jakob Slyngstadli
