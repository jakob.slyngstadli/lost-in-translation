import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext"
import "./Navbar.css"

const Navbar = () => {

    const { user } = useUser()

    return(
        <nav>
            <span className="navigationElement"><NavLink to="/">Lost in Translation</NavLink></span>
            
            { user !== null &&
                <ul className="mainNavigation">
                    <li className="navigationElement">
                        <NavLink to="/orders">Orders</NavLink>
                    </li>
                    <li className="navigationElement">
                        <NavLink to="/profile">Profile</NavLink>
                    </li>
                </ul>
            }
            
        </nav>
    )
}

export default Navbar