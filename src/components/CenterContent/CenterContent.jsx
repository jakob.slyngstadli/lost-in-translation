import "./CenterContent.css"

const CenterContent = ({ children }) => {
    return (
        <div className="centerContent">
            {children}
        </div>
    );
}

export default CenterContent;