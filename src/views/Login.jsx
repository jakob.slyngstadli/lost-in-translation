import CenterContent from '../components/CenterContent/CenterContent'
import LoginForm from '../components/Login/LoginForm'
import './Login.css'

const Login = () => {
    return(
        <>
            <header className="pageHeader">
                <CenterContent>
                    <div className="logoDiv">
                        <img src="img\LostInTranslation_Resources\Logo.png" className="logoMan" />
                        <div>
                            <h1>Lost in Translation</h1>
                            <h2>Get started</h2>
                        </div>
                    </div>
                </CenterContent>
                <CenterContent>
                    <div className="loginDiv">
                        <LoginForm />
                    </div>
                </CenterContent>
                
            </header>
            
        </>
            
    )
}
export default Login