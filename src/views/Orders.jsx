import OrdersCoffeeButton from "../components/Orders/OrderCoffeeButton"
import OrderForm from "../components/Orders/OrderForm"
import withAuth from "../hoc/withAuth"
import { useState } from "react"
import { useUser } from "../context/UserContext"
import { orderAdd } from "../api/order"
import { storageSave } from "../utils/storage"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import CenterContent from "../components/CenterContent/CenterContent"
import './Orders.css'

const signSigns = [
    {
        id: 'a',
        image:"img\LostInTranslation_Resources\individial_signs\a.png"
    },
    {
        id: 'b',
        image:"img\\LostInTranslation_Resources\\individial_signs\\b.png"
    },
    {
        id: 'c',
        image:"img\LostInTranslation_Resources\individial_signs\c.png"
    },
    {
        id: 'd',
        image:"img\LostInTranslation_Resources\individial_signs\d.png"
    },
    {
        id: 'e',
        image:"img\LostInTranslation_Resources\individial_signs\e.png"
    },
    {
        id: 'f',
        image:"img\LostInTranslation_Resources\individial_signs\f.png"
    },
    {
        id: 'g',
        image:"img\LostInTranslation_Resources\individial_signs\g.png"
    },
    {
        id: 'h',
        image:"img\LostInTranslation_Resources\individial_signs\h.png"
    },
    {
        id: 'i',
        image:"img\LostInTranslation_Resources\individial_signs\i.png"
    },
    {
        id: 'j',
        image:"img\LostInTranslation_Resources\individial_signs\j.png"
    },
    {
        id: 'k',
        image:"img\LostInTranslation_Resources\individial_signs\k.png"
    },
    {
        id: 'l',
        image:"img\LostInTranslation_Resources\individial_signs\l.png"
    },
    {
        id: 'n',
        image:"img\LostInTranslation_Resources\individial_signs\n.png"
    },
    {
        id: 'm',
        image:"img\LostInTranslation_Resources\individial_signs\m.png"
    },
    {
        id: 'o',
        image:"img\LostInTranslation_Resources\individial_signs\o.png"
    },
    {
        id: 'p',
        image:"img\LostInTranslation_Resources\individial_signs\p.png"
    },
    {
        id: 'q',
        image:"img\LostInTranslation_Resources\individial_signs\q.png"
    },
    {
        id: 'r',
        image:"img\LostInTranslation_Resources\individial_signs\r.png"
    },
    {
        id: 's',
        image:"img\LostInTranslation_Resources\individial_signs\s.png"
    },
    {
        id: 't',
        image:"img\LostInTranslation_Resources\individial_signs\t.png"
    },
    {
        id: 'u',
        image:"img\LostInTranslation_Resources\individial_signs\\u.png"
    },
    {
        id: 'v',
        image:"img\LostInTranslation_Resources\individial_signs\v.png"
    },
    {
        id: 'w',
        image:"img\LostInTranslation_Resources\individial_signs\w.png"
    },
    {
        id: 'x',
        image:"img\LostInTranslation_Resources\individial_signs\\x.png"
    },
    {
        id: 'y',
        image:"img\LostInTranslation_Resources\individial_signs\y.png"
    },
    {
        id: 'z',
        image:"img\LostInTranslation_Resources\individial_signs\z.png"
    }
]

const Orders = () => {

    const [coffee] = useState(null)
    const [imgState, setImgState] = useState([])
    const {user, setUser} = useUser()

    const handleOrderClicked = async (notes) => {
        if(notes === ""){
            alert('Enter a string')
            return
        }

        const order = (notes).trim()

        const [error, updatedUser] = await orderAdd(user, order)
        if(error !== null){

        }

        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
        console.log(notes);

        handleTranslation(notes)

        console.log('Error', error)
        console.log('updatedUser', updatedUser)
    }

    const handleTranslation = (notes) => {
        let imgArray= []
        for(let i = 0; i < notes.length; i++){
            let teller = 0;
            while(teller<signSigns.length) {
                if (signSigns[teller].id === notes.charAt(i).toLowerCase()) {
                    imgArray.push(signSigns[teller].image)
                    //imgArray[imgArray.length+1] = signSigns[teller].image
                    break;
                }
                teller++;
            } 
        }
        const toPrintable = imgArray.map((signImage, index) => {

            console.log(signImage);
            return <img src="${signImage}" key={index}/>
        })
        console.log(imgArray)
        setImgState(toPrintable)
    }
        

    // const availableCoffees = COFFEES.map(coffee => {
    //    return <OrdersCoffeeButton 
    //                key={coffee.id} 
    //                coffee={coffee}
    //                onSelect={handleCoffeeClicked}/>
    //})

    // <section id="coffee-options">
    //      {availableCoffees}
    // </section>
    return(
        <CenterContent>
            <div>
                <section id="order-notes" className="orderDiv">
                    <OrderForm onOrder={handleOrderClicked} />
                </section>
                <h4> Translation </h4>
                {coffee && <p>Selected coffee: {coffee.name}</p>}
                <section>
                    {imgState}
                </section>
            </div>
            
        </CenterContent>
    )
}
export default withAuth(Orders)